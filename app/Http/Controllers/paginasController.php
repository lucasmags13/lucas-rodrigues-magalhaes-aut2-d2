<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class paginasController extends Controller
{
    public function index(){
        return view('welcome');
    }
    
    public function listagemLivros(){
        $livros = \App\Models\Livros::with ('autor')->get();
        return view('listagemLivros')->with (compact('livros'));
    }
}
