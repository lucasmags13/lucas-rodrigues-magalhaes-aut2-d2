<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Livros extends Model

{
    
    public function autor()
    {
        return $this->belongTo('App\Models\Autores', 'id_autor', 'id');
    }
    public function editora()
    {
        return $this->belongTo('App\Models\Editoras', 'id_editora', 'id');
    }

}
