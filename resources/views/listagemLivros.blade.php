
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <h2>Listagem Livros</h2>
        <hr>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body>
        <div>
            
            <table>
                <th>
                    <td>Título</td>
                    <td>Autor</td>
                    <td>Editoras</td>
                    <td>Local</td>
                </th>
                @foreach ($livros as $livro)
                    <th>
                        <td>{{$livro->titulo}}</td>
                        <td>{{$livro->autor->nome}}</td>
                        <td>{{$livro->id_editora->nome}}</td>
                        <td>{{$livro->local}}</td>
                    </th>
                @endforeach
            </table>

       
        </div>
    </body>
</html>